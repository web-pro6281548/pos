import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import type { User } from '@/types/User'

export const useAuthStore = defineStore('auth', () => {
    const currentUser = ref<User>({
    id: 1,
    email: 'armya47@gmail.com',
    password: '0101',
    fullName: 'armya',
    gender: 'male',
    roles: ['user']
})

  return { currentUser }
})
